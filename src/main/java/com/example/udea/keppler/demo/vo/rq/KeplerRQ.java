package com.example.udea.keppler.demo.vo.rq;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class KeplerRQ implements Serializable {

    private String msg;
}
