package com.example.udea.keppler.demo.service.impl;

import com.example.udea.keppler.demo.service.KeplerService;
import com.example.udea.keppler.demo.service.VectorService;
import com.example.udea.keppler.demo.vo.rq.KeplerRQ;
import com.example.udea.keppler.demo.vo.rq.VectorRQ;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class VectorSericeImpl implements VectorService {

    @Override
    public String dot(VectorRQ rq) {
        if(rq.getA().size() == rq.getB().size() && rq.getA().size() == 3) {
            return rq.getA().get(0) * rq.getB().get(0) +
                    rq.getA().get(1) * rq.getB().get(1) +
                    rq.getA().get(2) * rq.getB().get(2) + "";
        } else {
            return "Not valid operation";
        }
    }

    @Override
    public String cross(VectorRQ rq) {
        return null;
    }
}
