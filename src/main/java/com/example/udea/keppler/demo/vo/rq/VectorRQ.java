package com.example.udea.keppler.demo.vo.rq;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
public class VectorRQ implements Serializable {

    private List<Integer> A;
    private List<Integer> B;

}
