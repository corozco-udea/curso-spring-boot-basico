package com.example.udea.keppler.demo.controller;

import com.example.udea.keppler.demo.service.KeplerService;
import com.example.udea.keppler.demo.service.VectorService;
import com.example.udea.keppler.demo.vo.rq.VectorRQ;
import com.example.udea.keppler.demo.vo.rq.KeplerRQ;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/kepler")
@AllArgsConstructor
public class KeplerController {

     private KeplerService keplerService;
     private VectorService vectorService;

    @GetMapping("/hello")
    public String hello_kepler() {
        return "hello kepler by Jorge Zuluaga";
    }

    @PostMapping("/hello_post")
    public String hello_kepler(@RequestBody final KeplerRQ rq) {
        return keplerService.hello(rq);
    }

    @PostMapping("/dot")
    public String dot_product(@RequestBody final VectorRQ rq) {
        return vectorService.dot(rq);
    }
}
