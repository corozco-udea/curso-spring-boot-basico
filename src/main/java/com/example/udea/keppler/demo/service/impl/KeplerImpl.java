package com.example.udea.keppler.demo.service.impl;

import com.example.udea.keppler.demo.service.KeplerService;
import com.example.udea.keppler.demo.vo.rq.KeplerRQ;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class KeplerImpl implements KeplerService {

    @Override
    public String hello(KeplerRQ rq) {
        return "Hello kepler by " + rq.getMsg() + " called from service";
    }
}
