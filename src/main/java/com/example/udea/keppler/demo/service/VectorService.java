package com.example.udea.keppler.demo.service;

import com.example.udea.keppler.demo.vo.rq.VectorRQ;
import org.springframework.stereotype.Service;

@Service
public interface VectorService {

    String dot(VectorRQ rq);

    String cross(VectorRQ rq);
}
