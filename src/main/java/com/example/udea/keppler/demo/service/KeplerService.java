package com.example.udea.keppler.demo.service;

import com.example.udea.keppler.demo.vo.rq.KeplerRQ;
import org.springframework.stereotype.Service;

@Service
public interface KeplerService {

    String hello(KeplerRQ rq);
}
